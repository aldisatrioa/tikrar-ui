<?php
require_once 'api_client.php';

class ApiController
{

    private $api_client;

    public function __construct()
    {
        $this->api_client = new ApiClient();
        $this->api_client->setLocalUrl('http://localhost/tikraracademy/wp-json/v1/api/');
        $this->api_client->setProductionUrl('http://tikraracademy.com/wp-json/v1/api/');
        $this->api_client->setMode(ApiClient::$LOCAL_MODE);
        $this->api_client->build();
    }

    public function get_api_client()
    {
        return $this->api_client;
    }

    function get_bank_for_confirmation_payment()
    {
        return $this->api_client->get('sygma-bank-for-confirmation')->get_body();
    }

    function get_sygma_bank_account()
    {
        return $this->api_client->get('sygma-bank-account')->get_body();
    }

    function confirm_payment($data)
    {
        return $this->api_client->post('confirm-payment', $data)->get_body();
    }

    function get_list_bank()
    {
        return $this->api_client->get('list-bank')->get_body();
    }

    function is_page_exists($token)
    {
        return $this->api_client->get("thank-you-exist?token=' . $token'")->get_body();
    }

    function get_tokens_by_token($token)
    {
        return $this->api_client->get("get-tokens-by-token?token=" . $token . "")->get_body();
    }

    function get_ship_by_member_id($member_id)
    {
        return $this->api_client->get("$member_id/get-ship-data")->get_body();
    }

    function get_member_info($member_id)
    {
        return $this->api_client->get("$member_id/member-info")->get_body();
    }

    function get_payment_confirmation($member_id)
    {
        return $this->api_client->get("$member_id/get-payment-confirmation-by-member")->get_body();
    }

    function get_province()
    {
        return $this->api_client->get("province")->get_body();
    }

    function get_city()
    {
        return $this->api_client->get("city")->get_body();
    }

    function get_district()
    {
        return $this->api_client->get("district")->get_body();
    }

    function get_product_price()
    {
        return $this->api_client->get("product-price")->get_body();
    }
}
