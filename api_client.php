<?php

class ApiClient
{

    public static $PRODUCTION_MODE = 'PRODUCTION_MODE';
    public static $LOCAL_MODE = 'LOCAL_MODE';

    public function __construct()
    {
    }

    public function setLocalUrl($localUrl)
    {
        $this->localUrl = $localUrl;
    }

    public function setProductionUrl($prodUrl)
    {
        $this->prodUrl = $prodUrl;
    }

    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    public function build()
    {
        if ($this->mode == self::$PRODUCTION_MODE) {
            $this->baseUrl = $this->prodUrl;
        } else {
            $this->baseUrl = $this->localUrl;
        }
    }

    public function post($endpoint, $data)
    {
        $response = wp_remote_post($this->baseUrl . $endpoint, array(
            'method' => 'POST',
            'timeout' => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'blocking' => true,
            'headers' => array(),
            'body' => $data,
            'cookies' => array()
        ));
        return $response;
    }

    public function get($endpoint)
    {
        $url = $this->baseUrl . $endpoint;
        $response = wp_remote_get($url);
        $this->response = $response;
        $this->url = $url;
        return $this;
    }

    public function get_full_url($url) {
        return $this->baseUrl . $url;
    }

    public function test() {
        return "asd";
    }

    public function get_url()
    {
        return $this->url;
    }

    public function get_body()
    {
        return json_decode($this->response['body'], true);
    }
}
