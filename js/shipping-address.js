jQuery(function($) {
  $("#progress").hide();

  var product_price_val = $("#product_price").text();
  $("#product_price").text(formatRupiah(product_price_val));

  var provinceChangeHandler = function() {
    selected_province = $("#province").val();
    var splitted_province = selected_province.split("-");
    var province_id = splitted_province[0];
    var province_name = splitted_province[1];

    var opt_city = changeProvince(province_id);
    $("#city")
      .html(opt_city.join(""))
      .change();
  };

  var cityChangeHandler = function() {
    selected_city = $("#city").val();
    var splitted_city = selected_city.split("-");
    var city_id = splitted_city[0];
    var city_name = splitted_city[1];

    var opt_district = changeCity(city_id);
    $("#district").html(opt_district.join(""));

    getShippingCost($("#district").val());
  };

  var districtChangeHandler = function() {
    getShippingCost($("#district").val());
  };

  $("#progress").hide();
  $("#fullname").keyup(function() {
    var txt = $(this).val();
    $(this).val(
      txt.replace(/^(.)|\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
    );
  });
  $("#address").keyup(function() {
    var txt = $(this).val();
    $(this).val(
      txt.replace(/^(.)|\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
    );
  });

  $("#province").change(provinceChangeHandler);
  $("#city").change(cityChangeHandler);
  $("#district").change(districtChangeHandler);

  function getShippingCost(district) {
    var splitted_district = district.split("-");
    var district_id = splitted_district[0];
    var district_name = splitted_district[1];

    $("#progress").show();
    $("#submit").prop('disabled', true);
    
    $.ajax({
      type: "POST",
      url: "http://localhost/tikraracademy/wp-json/v1/api/shipping-cost",
      data: {
        origin: 23,
        courier: "jne",
        district_id: district_id
      },
      success: function(data, success) {
        var ship_cost = data.shipping_cost;
        var total_cost = ship_cost + parseInt(product_price);
        $("#ongkir_cost").text(formatRupiah(ship_cost.toString()));
        $("input[name='ongkir_cost']").val(ship_cost);
        $("#total").html(formatRupiah(total_cost.toString()));
        $("input[name='total']").val(total_cost);

        $("#progress").hide();
        $("#submit").prop('disabled', false);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $("#progress").hide();
        $("#submit").prop('disabled', false);
      }
    });
  }

  function formatRupiah(angka) {
    var prefix = "Rp.";
    var number_string = angka.replace(/[^,\d]/g, "").toString();
    split = number_string.split(",");
    sisa = split[0].length % 3;
    rupiah = split[0].substr(0, sisa);
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }

    rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    var result = prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    return result;
  }

  function changeProvince(id) {
    var result = city.reduce((col, x) => {
      if (x.province_id == id) {
        var mixIdAndName = x.id + "-" + x.name;
        y = '<option value="' + mixIdAndName + '">' + x.name + "</option>";
        col.push(y);
      }
      return col;
    }, []);
    return result;
  }

  function changeCity(id) {
    var result = district.reduce((col, x) => {
      if (x.city_id == id) {
        var mixIdAndName = x.id + "-" + x.name;
        y = '<option value="' + mixIdAndName + '">' + x.name + "</option>";
        col.push(y);
      }
      return col;
    }, []);
    return result;
  }
});
