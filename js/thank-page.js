jQuery(function($) {

  $('#btn-konfirmasi').click(function() {
    window.location = direct_to;
  })
  $("#btn-copy-jumlah").click(function() {
    copyToClipboard("#price");
  });
  $("#btn-copy-rek").click(function() {
    copyToClipboard("#no-rekening");
  });

  function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
  }
});
