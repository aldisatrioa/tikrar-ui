jQuery(function($) {
  $("#btn-spinner").hide();
  $("#error-show").hide();

  var frm = $("#form");
  frm.submit(function(e) {
    e.preventDefault();
    $("#submit").prop("disabled", true);

    let ship_cost = $("input[name='ongkir_cost']");
    let total_cost = $("input[name='total']");

    if (ship_cost.val() != "" && total_cost.val() != "") {
      submit_registration();
    } else {
      $("#submit").prop("disabled", false);
      console.log("empty");
    }

    // checkEmail();
  });

  function submit_form() {
    $("#btn-spinner").show();
    $("#btn-submit").text("Mendaftarkan akun...");
    $("#submit").prop("disabled", true);

    var province_id = $("select[name='province']")
      .val()
      .split("-")[0];
    var province_name = $("select[name='province']")
      .val()
      .split("-")[1];
    var city_id = $("select[name='city']")
      .val()
      .split("-")[0];
    var city_name = $("select[name='city']")
      .val()
      .split("-")[1];
    var district_id = $("select[name='district']")
      .val()
      .split("-")[0];
    var district_name = $("select[name='district']")
      .val()
      .split("-")[1];

    $.ajax({
      type: "POST",
      url: register_receiver_url,
      data: {
        fullname: $("input[name='nama_lengkap']").val(),
        phone_number: $("input[name='no_wa']").val(),
        email: $("input[name='email']").val(),
        address: $("textarea[name='address']").val(),
        postcode: $("input[name='post_code']").val(),
        province_id: province_id,
        province_name: province_name,
        city_id: city_id,
        city_name: city_name,
        district_id: district_id,
        district_name: district_name,
        shipping_cost: $("input[name='ongkir_cost']").val(),
        total: $("input[name='total']").val()
      },
      success: function(data, success) {
        if (data.success) {
          window.location = direct_to + "/?token=" + data.code;
        }
        $("#btn-spinner").hide();
        $("#btn-submit").text("Selesaikan Order");
        $("#submit").prop("disabled", false);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $("#btn-spinner").hide();
        $("#submit").prop("disabled", false);
        $("#btn-submit").text("Selesaikan Order");
        $("#error-show").hide();
      }
    });
  }

  function check_nonce_validation() {
    $.ajax({
      type: "POST",
      url: ajax.ajaxurl,
      data: {
        action: "check_nonce_validation",
        nonce: $("#nonce").val()
      },
      success: function(data) {
        console.log(data);
      },
      error: function(errorThrown) {
        console.log(errorThrown);
      }
    });
  }

  function submit_registration() {
    checkEmail();
  }

  function checkEmail() {
    $("#btn-spinner").show();
    $("#btn-submit").text("Memvalidasi...");

    $.ajax({
      type: "POST",
      url: "http://localhost/tikraracademy/wp-json/v1/api/check-email",
      data: {
        email: $("#email").val()
      },
      success: function(data, success) {
        $("#submit").prop("disabled", false);
        $("#btn-submit").text("Selesaikan Order");
        $("#btn-spinner").hide();

        if (data.is_exist) {
          $("#error-show").show();
        } else {
          submit_form();
          $("#error-show").hide();
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $("#submit").prop("disabled", false);
        $("#btn-spinner").hide();
        $("#error-show").hide();
      }
    });
  }
});
