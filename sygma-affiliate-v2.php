<?php

/**
 * Plugin Name: Tikrar Form
 * Plugin URI: https://www.yourwebsiteurl.com/
 * Description: Untuk keperluan Form Tikrar
 * Version: 2.4.3
 * Author: Aldi Satrio Aji
 * Author URI: http://yourwebsiteurl.com/
 **/
// Make sure we don't expose any info if called directly

if (!function_exists('add_action')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

define('SYGMAAFFILIATE_VERSION', '2.4.3');
define('SYGMAAFFILIATE__MINIMUM_WP_VERSION', '4.0');
define('SYGMAAFFILIATE__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('SYGMAAFFILIATE_DELETE_LIMIT', 100000);

require_once(SYGMAAFFILIATE__PLUGIN_DIR . 'api_client.php');
require_once(SYGMAAFFILIATE__PLUGIN_DIR . 'functions.php');
require_once(SYGMAAFFILIATE__PLUGIN_DIR . 'class.sygmaaffiliate-client.php');
require_once(SYGMAAFFILIATE__PLUGIN_DIR . 'view.signup-form.php');
require_once(SYGMAAFFILIATE__PLUGIN_DIR . 'view.thank-you.php');
require_once(SYGMAAFFILIATE__PLUGIN_DIR . 'view.payment-confirmation.php');

register_activation_hook( __FILE__, array( 'SygmaAffiliateClient', 'plugin_activation' ) );

add_shortcode('register_code', 'register_form');
add_shortcode('thankyou_code', 'thank_you_form');
add_shortcode('payment_code', 'payment_form');