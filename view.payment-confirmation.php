<?php

function payment_form()
{
  wp_enqueue_style('jquery-theme', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
  wp_enqueue_style('bootstrap-ui', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');

  wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-1.12.4.js');
  wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js');

  $api_controller = new ApiController();

  $get_tokens_by_token = $api_controller->get_tokens_by_token($_GET['token']);
  $is_exists = $get_tokens_by_token['id'] != null;

  $member_id = $get_tokens_by_token['member_id'];
  $get_payment_info_by_member_id = $api_controller->get_payment_confirmation($member_id);
  $get_member_info_by_member_id = $api_controller->get_member_info($member_id);
  $get_ship_address = $api_controller->get_ship_by_member_id($member_id);

  $has_confirm = $get_payment_info_by_member_id['register_in_landing'] == 1;
  $is_paid = $get_member_info_by_member_id['is_paid'];

  $sygma_bank_account = $api_controller->get_sygma_bank_account();
  $send_to = $sygma_bank_account['no_rek'] . ' a/n : ' . $sygma_bank_account['atas_nama'] . ', ' . $sygma_bank_account['bank_name'];

  $bank_list = $api_controller->get_list_bank();

  if (isset($_POST['submit-confirmation'])) {
    $date = date_create($_POST['paid_date']);
    $data = [
      'to_bank_id' => $_POST['bank_to'],
      'from_bank_id' => $_POST['bank_from'],
      'from_bank_account_holder' => $_POST['bank_holder'],
      'paid_amount' => $_POST['jumlah'],
      'paid_date' => date_format($date, "Y/m/d"),
      'payment_receipt' => $_FILES['payment_receipt'],
      'payment_confirmed' => 0,
      'member_id' => $get_ship_address['member_id'],
      'upline_bonuses' => 0
    ];

    $api_controller->confirm_payment($data);

    echo "<meta http-equiv='refresh' content='0'>";
  }

?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <style type="text/css">
      select:required:invalid {
        color: gray;
      }

      option[value=""][disabled] {
        display: none;
      }

      option {
        color: black;
      }
    </style>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  </head>


  <?php if ($is_exists == 0) : ?>
    <div class="jumbotron text-center vh-100">
      <h1 class="display-5">Order tidak ditemukan!</h1>
      <p class="lead">Order yang anda cari tidak ditemukan.</p>
      <h1 class="display-1">404</h1>
    </div>
  <?php elseif ($is_paid == 1) : ?>
    <div class="jumbotron text-center vh-100">
      <h1 class="display-5">Akun Tikrar Academy anda kini telah aktif!</h1>
      <p class="lead">Silahkan cek E-mail anda, kami telah mengirimkan kata sandi untuk akun Tikrar Academy.</p>
      <h3 class="mb-3">Status Order: <span class="badge badge-success">Dikonfirmasi</span></h3>
      <a href="https://tikraracademy.com/member-login" class="btn btn-primary active" role="button" aria-pressed="true">Login Tikrar Academy</a>
    </div>
  <?php elseif ($has_confirm) : ?>
    <div class="jumbotron text-center vh-100">
      <h1 class="display-5">Terimakasih telah melakukan konfirmasi!</h1>
      <p class="lead">Saat ini order anda tengah menunggu konfirmasi dari Admin.</p>

      <h3 class="mb-3">Status Order : <span class="badge badge-info">Pending</span></h3>
    </div>
  <?php else : ?>
    <div class="wrapper container">
      <form method="post" id="form" enctype="multipart/form-data">
        <div class="row" style="padding: 16px;">
          <div class="col-md-6 mx-auto">
            <div>
              <h5>
                Konfirmasi Pembayaran
              </h5>
              <div class="form-group">
                <label for="exampleInputEmail1">Nomor Invoice</label>
                <input class="form-control" id="no_wa" placeholder="Nomor Invoice" name="no_wa" value="<?php echo '#' . $get_ship_address['id'] ?>" required readonly />
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Waktu Pembayaran</label>
                <input class="form-control" placeholder="Pilih Waktu Pembayaran" name="paid_date" id="paid_date" required />
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Ditransfer Ke</label>
                <input type="text" class="form-control" id="fullname" placeholder="Nama Lengkap" value="<?php echo $send_to; ?>" readonly required />
                <input type="hidden" name="bank_to" value="<?php echo $bank['bank_id']; ?>" readonly required />
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Bank Asal</label>
                <select class="form-control ship_select" id="province" name="bank_from" required>
                  <?php foreach ($bank_list as $row) : ?>
                    <option value="<?php echo $row['id'] . '-' . $row['bank_name']; ?>"><?php echo $row['bank_name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Pemilik Rekening</label>
                <input type="text" class="form-control" value="<?php echo $get_payment_info_by_member_id['from_bank_account_holder']; ?>" id="holder" placeholder="Nama Lengkap" name="bank_holder" required />
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Jumlah</label>
                <input type="text" class="form-control" id="fullname" value="<?php echo $get_payment_info_by_member_id['paid_amount']; ?>" placeholder="Nama Lengkap" name="jumlah" required readonly />
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Bukti Transfer (.jpeg/.png/)</label>
                <div class="custom-file">
                  <input type="file" value="<?php $get_payment_info_by_member_id['payment_receipt']; ?>" class="custom-file-input" name="payment_receipt" id="customFile" required>
                  <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
              </div>
            </div>
            <button id="submit" type="submit" name="submit-confirmation" class="btn btn-primary btn-block">
              <span id="btn-submit">Konfirmasi</span>
            </button>
          </div>
          <div class="col-md-4 mx-auto align-self-center">
            <div class="card">
              <div class="card-header">
                <h4>Status Order:</h4>
              </div>
              <div class="card-body">
                <?php if ($has_confirm) : ?> <h2 class="text-success"> Menunggu Konfirmasi </h2> <?php else : ?> <h2 class="text-danger">Belum Konfirmasi</h2><?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  <?php endif; ?>
  </body>

  </html>
  <script>
    $(function() {
      $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
      });
      $("#holder").keyup(function() {
        var txt = $(this).val();
        $(this).val(
          txt.replace(/^(.)|\s(.)/g, function($1) {
            return $1.toUpperCase();
          })
        );
      });
      $("#paid_date").datepicker({
        dateFormat: 'dd/mm/yy'
      }).datepicker("setDate", new Date());
    });
  </script>
<?php
  function hook_footer()
  {
    // wp_enqueue_script('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js');
    wp_enqueue_script('affiliate-shipping-address', plugin_dir_url(__FILE__) . 'js/shipping-address.js', array('jquery'), SYGMAAFFILIATE_VERSION);
    wp_enqueue_script('affiliate-submit-receive', plugin_dir_url(__FILE__) . 'js/submit-receive-form.js', array('jquery'), SYGMAAFFILIATE_VERSION);
  }

  add_action('wp_ajax_submit_receive_form', 'submit_register');
  add_action('wp_ajax_check_nonce_validation', 'check_nonce_validation');
  add_action('wp_footer', 'hook_footer');
}
