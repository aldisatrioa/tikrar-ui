 <?php

  function thank_you_form()
  {
    wp_enqueue_style('bootstrap-ui', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');

    $api_controller = new ApiController();

    $get_tokens_by_token = $api_controller->get_tokens_by_token($_GET['token']);
    $is_exists = $get_tokens_by_token['id'] != null;

    $get_sygma_bank_for_confirm = $api_controller->get_bank_for_confirmation_payment();

    $member_id = $get_tokens_by_token['member_id'];
    $get_payment_info_by_member_id = $api_controller->get_payment_confirmation($member_id);
    $get_member_info_by_member_id = $api_controller->get_member_info($member_id);

    $has_confirm = $get_payment_info_by_member_id['register_in_landing'] == 1;
    $is_paid = $get_member_info_by_member_id['is_paid'];
  ?>

   <body>
     <?php if ($is_exists == 0) : ?>
       <div class="jumbotron text-center vh-100">
         <h1 class="display-5">Order tidak ditemukan!</h1>
         <p class="lead">Order yang anda cari tidak ditemukan.</p>
         <h1 class="display-1">404</h1>
       </div>
     <?php elseif ($is_paid == 1) : ?>
       <div class="jumbotron text-center vh-100">
         <h1 class="display-5">Akun Tikrar Academy anda kini telah aktif!</h1>
         <p class="lead">Silahkan cek E-mail anda, kami telah mengirimkan kata sandi untuk akun Tikrar Academy.</p>
         <h3 class="mb-3">Status Order: <span class="badge badge-success">Dikonfirmasi</span></h3>
         <a href="https://tikraracademy.com/member-login" class="btn btn-primary active" role="button" aria-pressed="true">Login Tikrar Academy</a>
       </div>
     <?php elseif ($has_confirm) : ?>
       <div class="jumbotron text-center vh-100">
         <h1 class="display-5">Terimakasih telah melakukan konfirmasi!</h1>
         <p class="lead">Saat ini order anda tengah menunggu konfirmasi dari Admin.</p>

         <h3 class="mb-3">Status Order : <span class="badge badge-info">Pending</span></h3>
       </div>
     <?php else : ?>
       <div class="jumbotron text-center">
         <h1 class="display-5">Terimakasih sudah melakukan order!</h1>
         <i>Kami telah mengirim Invoice dan link pembayaran ke E-mail anda.</i>
         <p class="lead">
           Untuk menyelesaikan proses order, silahkan transfer sejumlah
         </p>
         <h1 class="text-success">Rp. <span id="price"><?php echo $get_payment_info_by_member_id['paid_amount']; ?></span>
         </h1>
         <button id="btn-copy-jumlah" style="margin-top: 16px;" class="btn btn-outline-primary btn-md">
           Salin Jumlah
         </button>
         <p style="margin-top: 32px;">
           Ke rekening bank berikut:
         </p>
         <div class="card" style="width:400px; margin: 0 auto;">
           <div class="card-body">
             <p class="card-text">Nama Bank: <b id="no-rekening"><?php echo $get_sygma_bank_for_confirm['bank_name']; ?></b></p>
             <p class="card-text">No. Rek: <b id="no-rekening"><?php echo $get_sygma_bank_for_confirm['no_rek']; ?></b></p>
             <p class="card-text">Atas Nama: <b><?php echo $get_sygma_bank_for_confirm['atas_nama']; ?></b></p>
             <button id="btn-copy-rek" class="btn btn-outline-primary btn-md">
               <span class="glyphicon glyphicon-print"></span>
               Salin No. Rekening
             </button>
           </div>
         </div>
         <div>
           <button id="btn-konfirmasi" class="btn btn-primary" style="margin-top: 32px;">
             Ke Konfirmasi Pembayaran
           </button>
         </div>
       </div>
     <?php endif; ?>
   </body>

   <script type="text/javascript">
     var direct_to = <?php echo "'" . home_url() . '/payment-confirmation/?token=' . $_GET['token'] . "'" ?>;
   </script>
 <?php
    function hook_footer2()
    {
      wp_enqueue_script('affiliate-thank-page', plugin_dir_url(__FILE__) . 'js/thank-page.js', array('jquery'), SYGMAAFFILIATE_VERSION);
    }
    add_action('wp_footer', 'hook_footer2');
  }
  ?>