<?php

function register_form()
{
  wp_enqueue_style('bootstrap-ui', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');

  $api_controller = new ApiController();

  $province = $api_controller->get_province();
  $city = $api_controller->get_city();
  $district = $api_controller->get_district();
  $product_price = $api_controller->get_product_price()['product_price'];
?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <style type="text/css">
      select:required:invalid {
        color: gray;
      }

      option[value=""][disabled] {
        display: none;
      }

      option {
        color: black;
      }
    </style>
    <meta charset="utf-8" />
  </head>

  <body>
    <div class="container">
      <form method="post" id="form">
        <div class="row" style="padding: 16px;">
          <div class="col-md">
            <input type="hidden" value="<?php echo wp_create_nonce("register-nonce") ?>" id="nonce" placeholder="Nonce" name="register-nonce" />
            <div>
              <h5>
                Data Penerima
              </h5>
              <div class="form-group">
                <input type="number" class="form-control" min="0" id="no_wa" placeholder="Nomor WhatsApp Aktif" name="no_wa" required />
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="email" placeholder="E-mail Aktif" name="email" required />
                <small><i>(E-mail akan digunakan untuk mengakses konten Tikrar Academy)</i></small>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="fullname" placeholder="Nama Lengkap" name="nama_lengkap" required />
              </div>
            </div>
            <div class="form-group">
              <select class="form-control ship_select" id="province" name="province" required>
                <option disabled selected>::Pilih Provinsi::</option>
                <?php foreach ($province as $row) : ?>
                  <option value="<?php echo $row['id'] . '-' . $row['name']; ?>"><?php echo $row['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group">
              <select class="form-control ship_select" name="city" id="city" required>
              </select>
            </div>
            <div class="form-group">
              <select class="form-control ship_select" name="district" id="district" required>
                <!-- <option disabled selected>Pilih Kecamatan</option> -->
              </select>
            </div>
            <div class="form-group">
              <input class="form-control" id="postcode" name="post_code" type="number" pattern="[0-9]*" min="0" placeholder="Kode Pos" required>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="address" id="address" placeholder="Isi Alamat Lengkap Disini.." rows="3" required></textarea>
            </div>

            <div id="error-show" class="alert alert-danger" role="alert">
              E-mail yang anda input sudah terdaftar di sistem.
            </div>

            <button id="submit" type="submit" name="submit-register" class="btn btn-primary">
              <span id="btn-spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
              <span id="btn-submit">Selesaikan Order</span>
            </button>
          </div>
          <div class="col-md">
            <div class="jumbotron d-flex align-items-center">
              <div class="container">
                <div class="row">
                  <div class="card">
                    <div class="card-header">
                      Rincian Harga
                      <small class="pull-right">
                        <span id="progress" class="spinner-border text-primary"></span>
                      </small>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-12">
                          <table class="table table-clear">
                            <tbody>
                              <tr>
                                <td class="left">
                                  Paket Pembelajaran Tikrar Academy
                                </td>
                                <td class="right">
                                  <input name="product_price" type="hidden" value="<?php echo $product_price; ?>" />
                                  <span id="product_price">
                                    <?php echo $product_price; ?>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="left">
                                  Tarif Pengiriman (JNE)
                                </td>
                                <td class="right">
                                  <input name="ongkir_cost" type="hidden" />
                                  <span id="ongkir_cost">
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="left">
                                  Total
                                </td>
                                <td class="right">
                                  <input name="total" type="hidden" />
                                  <strong id="total">
                                  </strong>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- <div class="row">
                <div class="card">
                  <div class="card-header">
                    Deskripsi
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        Transfer ke rekening : <br />
                        - Bank BCA: 437-152-489-9 an. Indra Laksana
                      </div>
                      <div class="border-top my-3"></div>
                      <div class="col-md-12">
                        Pengiriman menggunakan JNE
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </body>
  <script type="text/javascript">
    var city = <?php echo json_encode($city); ?>;
    var district = <?php echo json_encode($district); ?>;
    var product_price = <?php echo $product_price; ?>;
    var direct_to = <?php echo "'" . home_url() . '/thank-you' . "'" ?>;
    var register_receiver_url = "<?php echo $api_controller->get_api_client()->get_full_url('register-receiver'); ?>";
  </script>

  </html>
<?php
  function hook_footer()
  {
    // wp_enqueue_script('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js');
    wp_enqueue_script('affiliate-shipping-address', plugin_dir_url(__FILE__) . 'js/shipping-address.js', array('jquery'), SYGMAAFFILIATE_VERSION);
    wp_enqueue_script('affiliate-submit-receive', plugin_dir_url(__FILE__) . 'js/submit-receive-form.js', array('jquery'), SYGMAAFFILIATE_VERSION);
  }

  add_action('wp_ajax_submit_receive_form', 'submit_register');
  add_action('wp_ajax_check_nonce_validation', 'check_nonce_validation');
  add_action('wp_footer', 'hook_footer');
}
