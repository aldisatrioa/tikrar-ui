<?php

class MemberRepository
{
    private $db;

    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
    }

    private function register()
    {
        if (!empty($_POST['submit-registration']) && $_POST['submit-registration']) {
            $nonce = $_POST['nonce'];
            if (!wp_verify_nonce($nonce, 'signup-nonce')) {
                // This nonce is not valid.
                die('Security check');
            } else {

                //**** validate before creating member
                if ($this->validateRegistrant($_POST) === false) {
                    $this->set_flashmessage('error', 'Email yang anda input sudah terdaftar di sistem.');
                    wp_redirect(site_url('/member-signup'));
                    exit;
                }

                //**** Create member
                $hashedPassword = wp_hash_password($_POST['password']);
                $table_name = $this->db->prefix . "affiliate_members";
                $refcode = !empty($_SESSION['affiliate_refcode']) ? $_SESSION['affiliate_refcode'] : '';
                unset($_SESSION['affiliate_refcode']);
                $upline_id = $this->getUplineId($refcode);

                $data = [
                    'fullname' => $_POST['fullname'],
                    'phone_number' => $_POST['phone_number'],
                    'email' => $_POST['email'],
                    'password' => $hashedPassword,
                    'upline_id' => $upline_id,
                ];
                $this->db->insert($table_name, $data);

                //**** Update set affiliate code
                $lastid = $this->db->insert_id;
                $reference_code = substr(str_replace(' ', '', $_POST['fullname']), 0, 4) . $lastid;
                $this->db->update($table_name, ['ref_code' => $reference_code], ['id' => $lastid], ['%s'], ['%d']);

                $this->set_flashmessage('success', 'Kami telah mengirimkan tautan untuk mengkonfirmasi pendaftaran ini ke email anda');
                //***** email to registrant
                $token = wp_hash_password($result->email . 'purpose-confirm-registration-member-id-' . $result->id);
                $data = ['member_id' => $lastid, 'token' => $token, 'purpose' => 'confirm-registration'];
                $this->db->replace($this->db->prefix . 'affiliate_tokens', $data, ['%d', '%s', '%s']);

                $fullname = $_POST['fullname'];
                $to = $_POST['email'];
                $subject = 'Tikrar Academy: Konfirmasi pendaftaran';
                $url_site = site_url('/member-login?action=confirm_registration&token=' . rawurlencode('*%x' . $lastid . '|:|' . $token), 'https');
                $body = '<p>Assalamu\'alaikum kak ' . $fullname . ',</p>' .
                    '<p>Terimakasih sudah melakukan pendaftaran di Tikrar Academy.' .
                    '<br>Klik <a href="' . $url_site . '" target="_blank">DI SINI</a> untuk mengkonfirmasi pendaftaran.</p>' .
                    '<p>Wassalamu\'alaikum wr wb<br>' .
                    '<a href="https://www.tikraracademy.com">www.tikraracademy.com</a><br>' .
                    '<a href="tel:' . get_option('phone_number') . '">' . get_option('phone_number') . '</a></p>';
                $this->sendEmail($to, $subject, $body);
                //***** email to admin
                $this->sendEmailToAdmin('Pendaftaran member baru', 'Assalamu\'alaikum Admin,<br>' . $fullname . ' dengan alamat email ' . $to . ' melakukan pendaftaran');

                wp_redirect(site_url('/member-login'));
                exit;
            }
        }
    }

    private function getUplineId($refcode)
    {
        if (empty($refcode)) {
            return null;
        } else {
            $table_name = $this->db->prefix . 'affiliate_members';
            $sql = $this->db->prepare('SELECT id FROM ' . $table_name . ' WHERE ref_code = %s', [$refcode]);
            $upline_id = $this->db->get_var($sql);
            if (!empty($upline_id)) {
                return $upline_id;
            } else {
                return null;
            }
        }
    }

    public function validateRegistrant($request)
    {
        if (!empty($request['email'])) {
            $email = $request['email'];
            $table_name = $this->db->prefix . 'affiliate_members';
            $sql = $this->db->prepare('SELECT COUNT(1) FROM ' . $table_name . ' WHERE email = %s', [$email]);
            $already_exists = $this->db->get_var($sql);
            if ($already_exists >= 1) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }
}
