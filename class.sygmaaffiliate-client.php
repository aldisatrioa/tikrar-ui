<?php

class SygmaAffiliateClient
{
    public function __construct()
    {
        wp_enqueue_style('bootstrap', plugins_url('/css/bootstrap.min.css', __FILE__));

        wp_enqueue_script('ajax_custom_script',  get_stylesheet_directory_uri() . '/includes/ajax-javascript.js', array('jquery'));
        wp_localize_script('ajax_custom_script', 'ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
        $this->page_definition();
    }

    private static function page_definition()
    {
        // Information needed for creating the plugin's pages
        // @content as function name for page
        $page_definitions = array(
            'thank-you' => array(
                'title' => __('Thank You', 'sygma-affiliate'),
                'content' => '[thankyou_code]',
                'template_name' => 'view.thank-you'
            ),
            'payment-confirmation' => array(
                'title' => __('Payment Confirmation', 'sygma-affiliate'),
                'content' => '[payment_code]',
                'template_name' => 'view.payment-confirmation'
            ),
        );
        return $page_definitions;
    }

    private static function create_symlink()
    {
        $output = `ls -al`;
        $theme_dir = get_template_directory();
        $plugin_dir = plugin_dir_path(__FILE__);
        $plugin_view = $plugin_dir . '/views';
        if (is_dir($plugin_view)) {
            if ($the_dir = opendir($plugin_view)) {
                while (($file_name = readdir($the_dir)) !== false) {
                    if (preg_grep('/^([^.])/', [$file_name])) {
                        if (is_file($plugin_view . '/' . $file_name)) {
                            if (is_file($theme_dir . '/' . $file_name)) {
                                unlink($theme_dir . '/' . $file_name);
                            }
                            exec('ln -s ../../plugins/tikrar-ui/views/' . $file_name . ' ' . $theme_dir . '/' . $file_name);
                        }
                    }
                }
            }
        }

        if (is_dir($theme_dir . '/template-part')) {
            unlink($theme_dir . '/template-part');
        }
    }

    /**
     * Plugin activation hook.
     *
     * Creates all WordPress pages needed by the plugin.
     */
    public static function plugin_activation()
    {
        // Information needed for creating the plugin's pages
        $page_definitions = self::page_definition();
        self::create_symlink();

        foreach ($page_definitions as $slug => $page) {
            // Check that the page doesn't exist already
            $query = new WP_Query('pagename=' . $slug);
            if (!$query->have_posts()) {
                // Add the page using the data from the array above
                wp_insert_post(
                    array(
                        'post_content' => $page['content'],
                        'post_name' => $slug,
                        'post_title' => $page['title'],
                        'post_status' => 'publish',
                        'post_type' => 'page',
                        'page_template' => $page['template_name'],
                        'ping_status' => 'closed',
                        'comment_status' => 'closed',
                    )
                );
            }
        }
    }

    function is_nonce_valid()
    {
        $nonce = $_POST['register-nonce'];
        return wp_verify_nonce($nonce, 'register-nonce');
    }
}

$sygma_affiliate_client = new SygmaAffiliateClient();
